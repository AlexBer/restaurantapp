# Projet : Création de restaurant




# Installation

cp .env.exemple .env
Mettre à jour le .env
composer install
npm i
php artisan migrate
php artisan db:seed

Je n'ai pas fais de seeder pour les comptes, il faut en créer un (/register) et se connecter.


# Routes 

/restaurants : Liste des restaurants
/restaurants/create : Création d'un nouveau restaurant

# WIP

Rédaction des tests  / API
