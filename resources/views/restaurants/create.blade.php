<div class="container">
    <div class="row justify-content-center">
        <form method="post" action="{{ route('restaurants.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">Nom du restaurant :</label>
                <input type="text" class="form-control" name="name" required/>
            </div>
            @foreach ($days as $day)
                <p>{{$day->name}}</p>
                <button type="button" class="addDay" day="{{$day->id}}">Ajouter un horaire le {{$day->name}}</button>
                <div id="day{{$day->id}}">
                    <div class="form-group">
                        <label>Horaire d'ouverture: </label>
                        <input type="time" name="start[{{$day->id}}][]">
                        <label>Horaire de fermeture</label>
                        <input type="time" name="end[{{$day->id}}][]">
                    </div>
                </div>
            @endforeach
            <button type="submit" class="btn btn-primary">Créer le restaurant</button>
        </form>
    </div>
</div>
<script>
    Array.from(document.getElementsByClassName('addDay')).forEach(function(element) {
       element.addEventListener('click', function(elem) {
            console.log('click')
            const dayId = elem.target.getAttribute('day');
            const targetDiv = document.getElementById('day' + dayId);
            const newDay = '<div class="form-group"><label>Horaire d\'ouverture: </label><input type="time" name="start['+ dayId +'][]"><label>Horaire de fermeture</label><input type="time" name="end['+ dayId +'][]"></div>';
            targetDiv.innerHTML += newDay
       });
     });
 </script>
