@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if($restaurants)
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nom du restaurant</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($restaurants as $restaurant)
                    <tr>
                        <th scope="col">{{$restaurant->name}}</th>
                            <td scope="col">
                                <div class='btn-group'>
                                    <a href="{{ route('restaurants.show', [$restaurant->id]) }}" class='btn btn-default btn-xs'>Voir les horaires</a>
                                    @auth
                                    <a href="{{ route('restaurants.edit', [$restaurant->id]) }}" class='btn btn-default btn-xs'>Editer</a>
                                    {!! Form::open(['route' => ['restaurants.destroy', $restaurant->id], 'method' => 'delete']) !!}
                                    {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Voulez-vous supprimer le restaurant ?')"]) !!}
                                    {!! Form::close() !!}
                                    @endauth
                                </div>
                            </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
        Pas de restaurants sur l'application @auth <a href="{{ route('restaurants.create') }}" class='btn btn-default btn-xs'>Créer un restaurant</a> @endauth
        @endif
    </div>
</div>
@endsection
