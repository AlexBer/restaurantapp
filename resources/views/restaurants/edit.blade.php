<form method="post" action="{{ route('restaurants.update', $restaurant->id) }}">
    @csrf
    {{ method_field('PATCH') }}
    <div class="form-group">
        <input type="text" name="name" value="{{$restaurant->name}}" required>
    </div>
    @foreach($days as $day)
        <strong>{{ $day->name }} :</strong><br>
        <button type="button" class="addDay" day="{{$day->id}}">+</button>
        <div id="day{{$day->id}}">
            @foreach($day->restaurants as $restaurant)
              <div class="form-group">
              <input type="time"  name="start[{{$day->id}}][]" value="{{$restaurant->pivot->start}}">
                  <input type="time" name="end[{{$day->id}}][]" value="{{$restaurant->pivot->end}}">
                  <li>{{ $restaurant->pivot->start }} à {{ $restaurant->pivot->end ?? null }}</li>
              </div>
            @endforeach
        </div>
    @endforeach
    <button type="submit" class="btn btn-primary">Modifier le restaurant</button>
        </form>
        <script>
           Array.from(document.getElementsByClassName('addDay')).forEach(function(element) {
              element.addEventListener('click', function(elem) {
                const dayId = elem.target.getAttribute('day');
                const targetDiv = document.getElementById('day' + dayId);
                const newDay = '<div class="form-group"><input type="time" name="start['+ dayId +'][]"><input type="time" name="end['+ dayId +'][]"></div>';
                targetDiv.innerHTML += newDay
              });
            });
        </script>
