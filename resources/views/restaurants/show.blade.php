<h2>{{$restaurant->name}}</h2>
<h3>Horaires d'ouverture :</h3>
    <div class="row">

        @foreach($days as $day)
        <strong>{{ $day->name }} :</strong><br>
            @if($day->restaurants->count() > 0)
                <ul>
                @foreach($day->restaurants as $restaurant)
                    <li>{{ $restaurant->pivot->start }} à {{ $restaurant->pivot->end }}</li>
                @endforeach
                </ul>
            @else
            <div class="row">
                Fermé
            </div>
            @endif
        @endforeach
    </div>
