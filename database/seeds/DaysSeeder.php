<?php

use Illuminate\Database\Seeder;

class DaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days')->insert([
            ['name' => 'Lundi' ],
            ['name' => 'Mardi' ],
            ['name' => 'Mercredi' ],
            ['name' => 'Jeudi' ],
            ['name' => 'Vendredi' ],
            ['name' => 'Samedi' ],
            ['name' => 'Dimanche' ],
        ]);
    }
}
