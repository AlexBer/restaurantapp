<?php

use Illuminate\Database\Seeder;
// use InsertDaysSeeder;
// use insertDaysSeder

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DaysSeeder::class);
    }
}
