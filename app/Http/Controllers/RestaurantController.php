<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use Illuminate\Support\Facades\Log;
use App\Day;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::with('days')->get();
        return view('restaurants.index')->with(['restaurants' => $restaurants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Day::all();
        return view('restaurants.create')->with(['days' => $days]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $starts = $request->all()['start'];
        $ends = $request->all()['end'];
        $restaurant = new Restaurant();
        $restaurant->name = $request->input('name');
        $restaurant->save();
        foreach ($starts as $key => $array){
          foreach ($array as $k => $value) {
            if($value && $ends[$key][$k]) {
                $restaurant->days()->attach([$key => ['start' => $value, 'end' => $ends[$key][$k]]]);
            }
          }
        }
        return redirect('restaurants/'.$restaurant->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restaurant = Restaurant::find($id);
        $days = Day::with(['restaurants' => function ($query) use($id) {
            $query->where('restaurants.id', $id);
        }])->get();
        return view('restaurants.show')->with([
            'restaurant' => $restaurant,
            'days' => $days
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $days = Day::with(['restaurants' => function ($query) use($id) {
            $query->where('restaurants.id', $id);
        }])->get();
        $restaurant = Restaurant::find($id);
        return view('restaurants.edit', compact('days','restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $starts = $request->all()['start'];
        $ends = $request->all()['end'];
        $restaurant = Restaurant::find($id);
        $restaurant->name = $request->input('name');
        $restaurant->save();
        $restaurant->days()->detach();
        foreach ($starts as $key => $array){
          foreach ($array as $k => $value) {
            if($value && $ends[$key][$k]) {
                $restaurant->days()->attach([$key => ['start' => $value, 'end' => $ends[$key][$k]]]);
            }
          }
        }
        return redirect('restaurants/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Restaurant::destroy($id);
        return redirect('restaurants/');
    }
}
