<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Day;
use App\Hour;

class Restaurant extends Model
{
    use SoftDeletes;

    protected $table = 'restaurants';

    protected $fillable = ['name'];

    public function days() {
        return $this->belongsToMany(Day::class, Hour::class)->withPivot('start', 'end');
    }
}
