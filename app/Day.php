<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Restaurant;
use App\Hour;

class Day extends Model
{
    public function restaurants() {
        return $this->belongsToMany(Restaurant::class, Hour::class)->withPivot('id', 'start', 'end');
    }
}
