<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hour extends Model
{
    use SoftDeletes;

    protected $table = 'hours';

    protected $fillable = ['restaurant_id', 'day_id', 'start', 'end'];
}
